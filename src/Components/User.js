import React from 'react';
import { connect } from 'react-redux';
import { deleteUser, showDialog, setEditingUser } from '../redux/actions';
import { EDIT_USER_DIALOG } from './Dialogs/dialogConstants';

const User = ({ user, deleteUser, showDialog, setEditingUser }) => {
    return (
        <tr>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td onClick={() => { setEditingUser(user); showDialog(EDIT_USER_DIALOG); }}><i style={{cursor: 'pointer'}} className="fas fa-edit"></i></td>
            <td onClick={() => deleteUser(user.id)}><i style={{cursor: 'pointer'}} className="fas fa-trash-alt"></i></td>
        </tr>
    )
}

export default connect(
    null,
    { deleteUser, showDialog, setEditingUser },
)(User);
