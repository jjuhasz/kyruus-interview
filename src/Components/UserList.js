import React from 'react';
import { connect } from 'react-redux';
import { Table, Alert } from 'react-bootstrap';
import User from './User';
import EditUserDialog from './Dialogs/EditUserDialog.js';
import { EDIT_USER_DIALOG } from './Dialogs/dialogConstants';

const UserList = ({ users, dialogVisible }) => {
    if (users && users.length > 0) {
        return (
            <div>
                {dialogVisible && <EditUserDialog />}
                <Table striped bordered hover responsive variant="dark">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        { 
                            users.map((user) => <User key={user.id} user={user} />) 
                        }
                    </tbody>
                </Table>
            </div>
        )
    } else {
        return (<Alert variant="warning">No users: did you start your backend?</Alert>)
    }
};

const mapStateToProps = state => {
    const users = state.users.allUsers;
    const dialogs = state.dialogs.dialogs;
    return { users, dialogVisible: dialogs[EDIT_USER_DIALOG] };
};

export default connect(mapStateToProps)(UserList);
