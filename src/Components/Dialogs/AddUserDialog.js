import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ADD_USER_DIALOG } from './dialogConstants';
import { addUser, hideDialog } from '../../redux/actions';

const AddUserDialog = ({ dialogVisible, addUser, hideDialog }) => {
    const [ userName, setUserName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ validated, setValidated ] = useState(false);
    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
    
        setValidated(true);
        addUser(userName, email);
        hideDialog(ADD_USER_DIALOG);
    };
    return (
        <Modal show={ dialogVisible } onHide={ () => { hideDialog(ADD_USER_DIALOG) }}>
            <Modal.Header closeButton>
                <Modal.Title>Add new user</Modal.Title>
            </Modal.Header>
            <Form validated={validated} onSubmit={handleSubmit}>
                <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter name" value={ userName } onChange={ event => setUserName(event.target.value) } required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={ email } onChange={ event => setEmail(event.target.value) } required/>
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={ () => { hideDialog(ADD_USER_DIALOG) }}>Cancel</Button>
                    <Button type="submit" variant="primary">Add user</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
}

const mapStateToProps = state => {
    const dialogs = state.dialogs.dialogs;
    return { dialogVisible: dialogs[ADD_USER_DIALOG] };
}

export default connect(mapStateToProps, { addUser, hideDialog })(AddUserDialog);
