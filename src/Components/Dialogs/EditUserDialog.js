import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { EDIT_USER_DIALOG } from './dialogConstants';
import { patchUser, hideDialog } from '../../redux/actions';

const EditUserDialog = ({ user, dialogVisible, patchUser, hideDialog }) => {
    const [ userName, setUserName ] = useState(user.name);
    const [ email, setEmail ] = useState(user.email);
    const [ validated, setValidated ] = useState(false);
    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
    
        setValidated(true);
        patchUser({ name: userName, email, id: user.id });
        hideDialog(EDIT_USER_DIALOG);
    };
    return (
        <Modal show={ dialogVisible } onHide={ () => { hideDialog(EDIT_USER_DIALOG) }}>
            <Modal.Header closeButton>
                <Modal.Title>Edit user</Modal.Title>
            </Modal.Header>
            <Form validated={validated} onSubmit={handleSubmit}>
                <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter name" value={ userName } onChange={ event => setUserName(event.target.value) } required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={ email } onChange={ event => setEmail(event.target.value) } required/>
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={ () => { hideDialog(EDIT_USER_DIALOG) }}>Cancel</Button>
                    <Button type="submit" variant="primary">Save</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    );
}

const mapStateToProps = state => {
    const dialogs = state.dialogs.dialogs;
    const user = state.dialogs.user;
    return {
        user,
        dialogVisible: dialogs[EDIT_USER_DIALOG],
    };
}

export default connect(mapStateToProps, { patchUser, hideDialog })(EditUserDialog);
