import React from 'react';
import { connect } from 'react-redux';
import { showDialog } from '../redux/actions';
import { Button } from 'react-bootstrap';
import AddUserDialog from './Dialogs/AddUserDialog';
import { ADD_USER_DIALOG } from './Dialogs/dialogConstants';

const AddUser = ({ dialogVisible, showDialog }) => {
    return (
        <div className="row">
            { dialogVisible && <AddUserDialog /> }
            <div className="col-2">
                <Button variant="secondary" onClick={ () => { showDialog(ADD_USER_DIALOG) }}><i className="fa fa-user-plus"></i> Add New User</Button>
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    const dialogs = state.dialogs.dialogs;
    return {
        dialogVisible: dialogs[ADD_USER_DIALOG],
    };
}

export default connect(mapStateToProps, { showDialog })(AddUser);
