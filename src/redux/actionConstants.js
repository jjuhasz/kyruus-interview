export const ADD_USER = 'ADD_USER';
export const DELETE_USER = 'DELETE_USER';
export const PATCH_USER = 'PATCH_USER';
export const SET_USER_LIST = 'SET_USER_LIST';

export const SHOW_DIALOG = 'SHOW_DIALOG';
export const HIDE_DIALOG = 'HIDE_DIALOG';
export const SET_EDITING_USER = 'SET_EDITING_USER'
