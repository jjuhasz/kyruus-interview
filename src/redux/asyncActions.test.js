import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import * as actions from './actions';
import * as types from './actionConstants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mockUsers = [
    {
        id: "1",
        name: "Mayra Reichel",
        email: "Jeff7@gmail.com"
      },
      {
        id: "2",
        name: "Raymundo Gaylord",
        email: "Margie.OConnell@hotmail.com"
      },
      {
        id: "3",
        name: "Georgette O'Connell",
        email: "Rosanna61@gmail.com"
      },
      {
        id: "4",
        name: "Mayra Reichel",
        email: "Jadyn.Konopelski58@hotmail.com"
      },
];

describe('async actions', () => {
    beforeEach(() => {
        moxios.install();
    });
    afterEach(() => {
        moxios.uninstall();
    });

    it('creates a SET_USER_LIST when fetching users has succeeded', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: mockUsers,
            });
        });
        const expectedActions = [
            {
                type: types.SET_USER_LIST,
                payload: mockUsers,
            },
        ];
        const store = mockStore({ allUsers: [] });
        return store.dispatch(actions.fetchUserList()).then(
            () => {
                expect(store.getActions()).toEqual(expectedActions);
            }
        );
    });
    it('creates a DELETE_USER when deleting a user has succeeded', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: {},
            });
        });
        const expectedActions = [
            {
                type: types.DELETE_USER,
                payload: {id: "2"},
            },
        ];
        const store = mockStore({ allUsers: [] });
        return store.dispatch(actions.deleteUser("2")).then(
            () => {
                expect(store.getActions()).toEqual(expectedActions);
            }
        );
    });
    it('creates an ADD_USER when adding a user has succeeded', () => {
        const user = mockUsers[0];
        
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: user,
            });
        });
        const expectedActions = [
            {
                type: types.ADD_USER,
                payload: user,
            },
        ];
        const store = mockStore({ allUsers: [] });
        return store.dispatch(actions.addUser({name: user.name, email: user.email})).then(
            () => {
                expect(store.getActions()).toEqual(expectedActions);
            }
        );
    });
    it('creates a PATCH_USER when modifying a user has succeeded', () => {
        const user = mockUsers[1];
        
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: {},
            });
        });
        const expectedActions = [
            {
                type: types.PATCH_USER,
                payload: user,
            },
        ];
        const store = mockStore({ allUsers: [] });
        return store.dispatch(actions.patchUser(user)).then(
            () => {
                expect(store.getActions()).toEqual(expectedActions);
            }
        );
    });
})