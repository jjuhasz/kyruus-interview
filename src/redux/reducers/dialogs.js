import { SHOW_DIALOG, HIDE_DIALOG, SET_EDITING_USER } from '../actionConstants';
import { ADD_USER_DIALOG, EDIT_USER_DIALOG } from '../../Components/Dialogs/dialogConstants';

const initialState = {
    dialogs: {
        [ADD_USER_DIALOG]: false,
        [EDIT_USER_DIALOG]: false,
    },
    user: {
        name: '',
        email: '',
        id: '',
    }
};

export default function(state = initialState, action) {
    switch(action.type) {
        case SHOW_DIALOG: {
            const dialogId = action.payload;
            if (state.dialogs.hasOwnProperty(dialogId)) {
                return {
                    ...state,
                    dialogs: {
                        ...state.dialogs,
                        [dialogId]: true,
                    }
                }
            }
            return state;
        }
        case HIDE_DIALOG: {
            const dialogId = action.payload;
            if (state.dialogs.hasOwnProperty(dialogId)) {
                return {
                    ...state,
                    dialogs: {
                        ...state.dialogs,
                        [dialogId]: false,
                    }
                }
            }
            return state;
        }
        case SET_EDITING_USER: {
            const user = action.payload;
            return {
                ...state,
                user,
            }
        }
        default:
            return state;
    }
}
