import reducer from './dialogs';
import * as types from '../actionConstants';
import * as dialogIds from '../../Components/Dialogs/dialogConstants';

const initialState = {
    dialogs: {
        [dialogIds.ADD_USER_DIALOG]: false,
        [dialogIds.EDIT_USER_DIALOG]: false,
    },
    user: {
        name: '',
        email: '',
        id: '',
    }
};

describe('dialogs reducer', () => {
    it('should have a valid initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    });
    it('should set a dialog as shown', () => {
        const expectedState = {
            ...initialState,
            dialogs: {
                ...initialState.dialogs,
                [dialogIds.ADD_USER_DIALOG]: true,
            },
        };
        const newState = reducer(initialState, {
            type: types.SHOW_DIALOG,
            payload: dialogIds.ADD_USER_DIALOG,
        });
        expect(newState).toEqual(expectedState);
    });
    it('should set a dialog as hidden', () => {
        const newInitState = {
            ...initialState,
            dialogs: {
                ...initialState.dialogs,
                [dialogIds.ADD_USER_DIALOG]: true,
            },
        };
        const newState = reducer(newInitState, {
            type: types.HIDE_DIALOG,
            payload: dialogIds.ADD_USER_DIALOG,
        });
        expect(newState).toEqual(initialState);
    });
    it('should set the user info that is currently being edited', () => {
        const editingUser = {
            name: 'John',
            email: 'john@foo.com',
            id: '1',
        }
        const expectedState = {
            ...initialState,
            user: editingUser,
        };
        const newState = reducer(initialState, {
            type: types.SET_EDITING_USER,
            payload: editingUser,
        });
        expect(newState).toEqual(expectedState);
    })
});