import _ from 'lodash';
import { ADD_USER, DELETE_USER, SET_USER_LIST, PATCH_USER } from '../actionConstants';

const initialState = {
    allUsers: [],
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_USER_LIST: {
            const users = action.payload;
            return {
                ...state,
                allUsers: users,
            };
        }
        case ADD_USER: {
            const user = action.payload;
            const allUsers = [ ...state.allUsers ];
            allUsers.push(user);
            return {
                ...state,
                allUsers,
            }
        }
        case DELETE_USER: {
            const { id } = action.payload;
            const idx = _.findIndex(state.allUsers, [ 'id', id ]);
            if (idx >= 0) {
                const allUsers = [ ...state.allUsers ];
                allUsers.splice(idx, 1);
                return {
                    ...state,
                    allUsers
                };
            }
            return state;
        }
        case PATCH_USER: {
            const user = action.payload;
            const idx = _.findIndex(state.allUsers, [ 'id', user.id ]);
            if (idx >= 0) {
                const allUsers = [ ...state.allUsers ];
                allUsers.splice(idx, 1, user);
                return {
                    ...state,
                    allUsers,
                };
            }
            return state;
        }
        default: 
            return state;
    }
}
