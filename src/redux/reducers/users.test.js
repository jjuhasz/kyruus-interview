import reducer from './users';
import * as types from '../actionConstants';

const initialState = {
    allUsers: [],
};

const mockUsers = [
    {
        id: "1",
        name: "Mayra Reichel",
        email: "Jeff7@gmail.com"
      },
      {
        id: "2",
        name: "Raymundo Gaylord",
        email: "Margie.OConnell@hotmail.com"
      },
      {
        id: "3",
        name: "Georgette O'Connell",
        email: "Rosanna61@gmail.com"
      },
      {
        id: "4",
        name: "Mayra Reichel",
        email: "Jadyn.Konopelski58@hotmail.com"
      },
];

describe('users reducer', () => {
    it('should have an initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle setting user list', () => {
        const newState = reducer(initialState, {
            type: types.SET_USER_LIST,
            payload: mockUsers,
        });
        expect(newState).toEqual({
            allUsers: mockUsers,
        });
    });
    it('should handle adding a user', () => {
        const newState = reducer(initialState, {
            type: types.ADD_USER,
            payload: mockUsers[0],
        });
        expect(newState).toEqual({
            allUsers: [mockUsers[0]]
        });
    });
    it('should handle deleting a user', () => {
        const state = {allUsers: mockUsers};
        const newState = reducer(state, {
            type: types.DELETE_USER,
            payload: {id: "4"},
        });
        const expectedState = {allUsers: [...mockUsers]};
        expectedState.allUsers.splice(3, 1);
        expect(newState).toEqual(expectedState);
    });
    it('should handle modifying a user', () => {
        const state = {allUsers: mockUsers};
        const modifiedUser = {
            ...mockUsers[0],
            name: "John",
            email: "john@foo.com",
        };
        const newState = reducer(state, {
            type: types.PATCH_USER,
            payload: modifiedUser,
        });
        const expectedState = {allUsers: [...mockUsers]};
        expectedState.allUsers[0].name = "John";
        expectedState.allUsers[0].email = "john@foo.com";
        expect(newState).toEqual(expectedState);
    });
});