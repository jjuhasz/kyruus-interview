import axios from 'axios';
import { SET_USER_LIST, DELETE_USER, SHOW_DIALOG, HIDE_DIALOG, ADD_USER, SET_EDITING_USER, PATCH_USER } from './actionConstants';
import { fillHostname } from '../utils';

export const fetchUserList = () => {
    return (dispatch) => {
        return axios.get(fillHostname('http://{hostname}/users')).then(
            (response) => {
                dispatch(setUserList(response.data));
            },
            () => {
                dispatch(setUserList([]));
            }
        );
    };
};

export const setUserList = (users) => {
    return {
        type: SET_USER_LIST,
        payload: users,
    };
};

export const addUser = (name, email) => {
    return (dispatch) => {
        return axios.post(fillHostname('http://{hostname}/users'),
        {
            name,
            email,    
        }).then(
            (response) => {
                const newUser = response.data;
                if (newUser && newUser.hasOwnProperty('id')) {
                    dispatch(addUserToStore(newUser));
                }
            },
            () => {
                console.log(`Could not add user: ${name}`);
            }
        )
    }
};

export const addUserToStore = (user) => {
    return {
        type: ADD_USER,
        payload: user,
    };
};

export const deleteUser = (id) => {
    return (dispatch) => {
        return axios.delete(fillHostname(`http://{hostname}/users/${id}`)).then(
            () => {
                // server does not return success: true/false
                dispatch(deleteUserFromStore(id));
            }
        )
        .catch(error => console.log(`Could not delete user: ${id}`));
    }
};

export const deleteUserFromStore = (id) => {
    return {
        type: DELETE_USER,
        payload: { id },
    };
};

export const patchUser = (user) => {
    return (dispatch) => {
        return axios.patch(fillHostname(`http://{hostname}/users/${user.id}`), {
            name: user.name,
            email: user.email,
        }).then(
            () => {
                dispatch(patchUserInStore(user));
            },
            () => {
                console.log(`Could not edit user: ${user.name}`);
            }
        )
    }
};

export const patchUserInStore = (user) => {
    return {
        type: PATCH_USER,
        payload: user,
    }
};

export const showDialog = (dialogId) => {
    return {
        type: SHOW_DIALOG,
        payload: dialogId,
    }
};

export const hideDialog = (dialogId) => {
    return {
        type: HIDE_DIALOG,
        payload: dialogId,
    }
};

export const setEditingUser = (user) => {
    return {
        type: SET_EDITING_USER,
        payload: user,
    };
};
