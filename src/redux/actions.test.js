import * as actions from './actions';
import * as types from './actionConstants';

const mockUsers = [
    {
        id: "1",
        name: "Mayra Reichel",
        email: "Jeff7@gmail.com"
      },
      {
        id: "2",
        name: "Raymundo Gaylord",
        email: "Margie.OConnell@hotmail.com"
      },
      {
        id: "3",
        name: "Georgette O'Connell",
        email: "Rosanna61@gmail.com"
      },
      {
        id: "4",
        name: "Mayra Reichel",
        email: "Jadyn.Konopelski58@hotmail.com"
      },
]

describe('basic actions', () => {
    it('should create a SHOW_DIALOG action to show a dialog', () => {
        const expectedAction = {
            type: types.SHOW_DIALOG,
            payload: 'MY_DIALOG',
        };
        expect(actions.showDialog('MY_DIALOG')).toEqual(expectedAction);
    });
    it('should create a HIDE_DIALOG action to hide a dialog', () => {
        const expectedAction = {
            type: types.HIDE_DIALOG,
            payload: 'MY_DIALOG',
        };
        expect(actions.hideDialog('MY_DIALOG')).toEqual(expectedAction);
    });
    it('should create an ADD_USER action to add a user to the store', () => {
        const user = mockUsers[0];
        const expectedAction = {
            type: types.ADD_USER,
            payload: user,
        };
        expect(actions.addUserToStore(user)).toEqual(expectedAction);
    });
    it('should create a DELETE_USER action to delete a user from the store', () => {
        const expectedAction = {
            type: types.DELETE_USER,
            payload: {id: 'user_id'},
        };
        expect(actions.deleteUserFromStore('user_id')).toEqual(expectedAction);
    });
    it('should create a PATCH_USER action to modify a user in the store', () => {
        const user = mockUsers[1];
        const expectedAction = {
            type: types.PATCH_USER,
            payload: user,
        };
        expect(actions.patchUserInStore(user)).toEqual(expectedAction);
    });
    it('should create a SET_USER_LIST action to initialize the user list', () => {
        const expectedAction = {
            type: types.SET_USER_LIST,
            payload: mockUsers,
        };
        expect(actions.setUserList(mockUsers)).toEqual(expectedAction);
    });
});