const isDev = () => process.env.NODE_ENV === 'development';

const LOCALHOST_SERVER = 'localhost:3001';

export const fillHostname = (url) => {
    if (isDev()) {
        return url.replace('{hostname}', LOCALHOST_SERVER);
    } else {
        return url.replace('{hostname}', window.location.hostname);
    }
}