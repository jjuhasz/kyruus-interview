import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import UserList from './Components/UserList';
import AddUser from './Components/AddUser';
import { fetchUserList } from './redux/actions';

class App extends Component {
  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col">
            <AddUser />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <UserList />
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.props.fetchUserList();
  }
}

export default connect(null, { fetchUserList })(App);
